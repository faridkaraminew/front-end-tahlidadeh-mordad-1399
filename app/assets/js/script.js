/* -------------------------------- Navigation -------------------------------- */

let isClose = true;

function onToggle() {
    const menu = document.getElementById('nav');
    if (isClose) {
        menu.classList.add('active');
        isClose = false;
    } else {
        menu.classList.remove('active');
        isClose = true;
    }
}

/* -------------------------------- Navigation -------------------------------- */

/* -------------------------------- Slideshow -------------------------------- */

let currentSlide = 0;
function func() {
    const itemList = $('.slideshow .items .item');
    for (let i = 0; i < itemList.length; i++) { // remove each element has active class
        itemList[i].classList.remove('active');
    }

    if (currentSlide < itemList.length - 1) {
        currentSlide = currentSlide + 1;
    } else {
        currentSlide = 0;
    }
    itemList[currentSlide].classList.add('active');
}

// قانون

setInterval(func, 2000); // function, duration

/* -------------------------------- Slideshow -------------------------------- */

// Library, Jquery => $
function $(selector) {
    return document.querySelectorAll(selector);
}